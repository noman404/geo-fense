package com.slate.assignment;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

public class Constants {

    public static final String GEO_FENSE_LOCATION_DEFAULT = "TARGETED_LOCATION";
    public static final float GEOFENCE_RADIUS_IN_METERS = 100;

    public static final HashMap<String, LatLng> AREA_LANDMARKS = new HashMap<>();

    static {
        AREA_LANDMARKS.put(GEO_FENSE_LOCATION_DEFAULT, new LatLng(3.1579432,101.7124128));
    }
}
