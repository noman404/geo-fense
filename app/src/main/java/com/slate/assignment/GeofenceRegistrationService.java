package com.slate.assignment;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

public class GeofenceRegistrationService extends IntentService {


    private static final String TAG = "GeoIntentService";

    public GeofenceRegistrationService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            Log.d(TAG, "GeofencingEvent error " + geofencingEvent.getErrorCode());
        } else {
            int transaction = geofencingEvent.getGeofenceTransition();
            List<Geofence> geofences = geofencingEvent.getTriggeringGeofences();
            Geofence geofence = geofences.get(0);
            if (transaction == Geofence.GEOFENCE_TRANSITION_ENTER) {
                Log.e(TAG, "You are inside of the Geofence");
                Toast.makeText(this, "You are inside of the Geofence", Toast.LENGTH_LONG).show();
            } else if(transaction == Geofence.GEOFENCE_TRANSITION_EXIT) {
                Log.e(TAG, "You are outside of the Geofence");
                Toast.makeText(this, "You are outside of the Geofence", Toast.LENGTH_LONG).show();
            }else if(transaction == Geofence.GEOFENCE_TRANSITION_DWELL){
                Log.e(TAG, "You're Moving within the Geofence");
                Toast.makeText(this, "You're Moving within the Geofence", Toast.LENGTH_LONG).show();

            }
        }
    }

}