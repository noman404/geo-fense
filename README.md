# geo-fense

# Basic Features
- Geo fense detection of a circular geofense
- Detect Wifi signal enable and disabling and considered them is inside or outside based on the geo fense and wifi pair requirement

# Instruction
- Map key already in project but paired with studio SHA1
- Open project and run onto device or emulator
- the Map will appear with a markar and shows the radios of the Geofense manually can mock location to device or emulator | walk with the device to outside of geofense area to get result of the Inside or Outside of the Geo fense
- Turn on/off wifi to get the result of the Inside or Outside of the Geo fense
